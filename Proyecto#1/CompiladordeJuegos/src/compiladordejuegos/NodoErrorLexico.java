/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladordejuegos;

/**
 *
 * @author Alter
 */
public class NodoErrorLexico {

    private String Lexema;
    private int Fila;
    private int Columna;
    
    public NodoErrorLexico(String lexema,int fila,int columna) {
        this.Lexema=lexema;
        this.Fila=fila;
        this.Columna=columna;
    }

    public String getLexema() {
        return Lexema;
    }

    public int getFila() {
        return Fila;
    }

    public int getColumna() {
        return Columna;
    }
    
}
