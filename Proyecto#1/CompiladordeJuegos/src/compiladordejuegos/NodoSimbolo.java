/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladordejuegos;


/**
 *
 * @author Alter
 */
public class NodoSimbolo {

    private String Lexema;
    private int Fila;
    private int Columna;
    private String Token;
    
    public NodoSimbolo(String lexema,int fila,int columna,String token) {
        this.Lexema=lexema;
        this.Fila=fila;
        this.Columna=columna;
        this.Token=token;
    }
    

    public String getLexema() {
        return Lexema;
    }

    public int getFila() {
        return Fila;
    }

    public int getColumna() {
        return Columna;
    }

    public String getToken() {
        return Token;
    }

    public void setLexema(String Lexema) {
        this.Lexema = Lexema;
    }

    public void setFila(int Fila) {
        this.Fila = Fila;
    }

    public void setColumna(int Columna) {
        this.Columna = Columna;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }
    
    
    
}
