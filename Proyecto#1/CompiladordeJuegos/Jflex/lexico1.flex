
//* ------------Secci�n codigo-usuario -------- */
package compiladordejuegos;

import java_cup.runtime.*;
import java.io.FileWriter;
import java.io.PrintWriter;

%%
/*- Secci�n de opciones y declaraciones -*/
/*-OPCIONES --*/
/* Nombre de la clase generada para el analizador lexico */
%class Lexico1
/*Si se quiere que no se tome en cuenta la diferencia entre la mayuscula y la minuscula*/
//%ignorecase
%caseless 
/*Para que el lector del archivo tenga la cadena de 16 bits*/
%unicode
/*Para que las clases sean publicas*/
//%public
/* Indicar funcionamiento autonomo, crea un metodo main para que solo sea llamando*/
//%standalone
/* Acceso a la columna y fila actual de analisis CUP */
%line
%column
/*El nombre de la clase que cup va a generar*/
%cupsym Simbolo1
/* Habilitar la compatibilidad con el interfaz CUP para el generador sintactico*/
%cup

%function next_token
%type java_cup.runtime.Symbol
%eofval{
	System.out.println("Fin");
        return new Symbol(Simbolo.EOF);
  
  //return null;
%eofval}
%eofclose

/*-- DECLARACIONES --*/
%{
	//String TextoPintado;
	String TextoErrores;
	int i;

	/* public String RetornarTextoPintado(){
		return TextoPintado;
	} */
	
	/* private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    } */
    
    /* Also creates a new java_cup.runtime.Symbol with information
       about the current token, but this object has a value. */
    /* private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    } */
	
	public void TablaErroes(){
        try{ 
            try (FileWriter Archivo = new FileWriter("ErrorTokens.html")) {
                PrintWriter Escribir = new PrintWriter(Archivo);

                    Escribir.println("<html>");
                    Escribir.println("<head><title>Reporte De Errores</title></head>");
                    Escribir.println("<body bgcolor=white text=Black background=FondoEj3.gif >");
                    Escribir.println("<h1><center>Reporte de Errores</center></h1>");
                    Escribir.println("<h2><center>Tokens invalidos</center></h2>");
                    Escribir.println("<br>");
                    Escribir.println("En la siguiente tabla se le presentara los tokens invalidos y su posicion en el texto. <br><br>");
                    Escribir.println("<center>");
                    Escribir.println("<table border= 1 width= 500 bgcolor= gray>");
                    Escribir.println("<tr>");
                    Escribir.println("<th>No.</th>");
                    Escribir.println("<th>Lexema</th>");
                    Escribir.println("<th>Fila</th>");
                    Escribir.println("<th>Columna</th>");
                    Escribir.println("</tr>");
                    Escribir.println(TextoErrores);
                    Escribir.println("</table><br>");
                    Escribir.println("</table><br><h2>Reporte Creado por 201114771</h2>");
                    Escribir.println("</center></body></html>");
                  Archivo.close();
     
            }

        } 
        catch(Exception e){
            
        }
    }
%}

%init{ 
	// TextoPintado="";
	TextoErrores="";
	i=0;
%init}

/*-- MACRO DECLARACIONES --*/
Espacio=[ \t]
Salto=\n
Num=[0-9]
Letras=[a-zA-Z]
Dec ={Num}+("."{Num}+)?
ID={Letras}({Num}|{Letras}|[_])*
//ID2=({Num}|{Letras})+
Carpeta=[^\\/:*?<>|\n \t][^\\/:*?<>|\n\t]*
Extension=("jpg"|"png"|"gif")
Directorio = [\"]({Letras}[:])? [\\]({Carpeta}([\\]{Carpeta})*)?{ID}{Extension}[\"]
Contenido=[\"][^\"]*[\"]



%%
/*-------- Secci�n de reglas lexicas ------ */
<YYINITIAL> {
	{Espacio} 	
		{
			System.out.println("Vino espacio");
			TextoPintado=TextoPintado+" ";
		}
	
	{Salto}		
		{
			TextoPintado=TextoPintado+"<br>";
			System.out.println("Vino Salto");
		}
	{ID}		
		{
			System.out.println("Vino ID");
		}
	/* {ID2}		
		{
			System.out.println("Vino ID2");
			TextoPintado=TextoPintado+"<font color=\"yellow\">"+yytext()+"<font>";
			return new Symbol(Simbolo.ID, yychar,yyline,new String(yytext()));
		}	 */
	/* {Carpeta}		
		{
			System.out.println("Vino carpeta");
		} */	
	{Directorio}		
		{
			System.out.println("Vino Directorio");
			String Temp=yytext();
			Temp=Temp.replaceAll("<", "&lt;");
            Temp=Temp.replaceAll(">", "&gt;");
			TextoPintado=TextoPintado+"<font color=\"yellow\">"+Temp+"<font>";
			return new Symbol(Simbolo.Dir, yychar,yyline,new String(yytext()));
		}
	{Contenido}		
		{
			System.out.println("Vino Contenido");
			String Temp=yytext();
			Temp=Temp.replaceAll("<", "&lt;");
            Temp=Temp.replaceAll(">", "&gt;");
			TextoPintado=TextoPintado+"<font color=\"yellow\">"+Temp+"<font>";
			return new Symbol(Simbolo.Con, yychar,yyline,new String(yytext()));
		}
	"<configuration>"		
		{
			System.out.println("Vino IEstructura");
			TextoPintado=TextoPintado+"<font color=\"blue\">&#60estructura&#62<font>";
			return new Symbol(Simbolo.IEstruc, yychar,yyline,new String(yytext()));
		}
	"</configuration>"		
		{
			System.out.println("Vino FEstructura");
			TextoPintado=TextoPintado+"<font color=\"blue\">&#60/estructura&#62<font>";
			return new Symbol(Simbolo.FEstruc, yychar,yyline,new String(yytext()));
		}
	"<background>"		
		{
			System.out.println("Vino Idirectorio");
			TextoPintado=TextoPintado+"<font color=\"black\">&#60directorio&#62<font>";
			return new Symbol(Simbolo.IDirec, yychar,yyline,new String(yytext()));
		}
	"</background>"		
		{
			System.out.println("Vino Fdirectorio");
			TextoPintado=TextoPintado+"<font color=\"black\">&#60/directorio&#62<font>";
			return new Symbol(Simbolo.FDirec, yychar,yyline,new String(yytext()));
		}
	"<figure>"		
		{
			System.out.println("Vino Icarpeta");
			TextoPintado=TextoPintado+"<font color=\"green\">&#60carpeta&#62<font>";
			return new Symbol(Simbolo.ICar, yychar,yyline,new String(yytext()));
		}
	"</figure>"		
		{
			System.out.println("Vino Fcarpeta");
			TextoPintado=TextoPintado+"<font color=\"green\">&#60/carpeta&#62<font>";
			return new Symbol(Simbolo.FCar, yychar,yyline,new String(yytext()));
		}
	"<design>"		
		{
			System.out.println("Vino Inombre");
			TextoPintado=TextoPintado+"<font color=\"gray\">&#60nombre&#62<font>";
			return new Symbol(Simbolo.INom, yychar,yyline,new String(yytext()));
		}
	"</design>"		
		{
			System.out.println("Vino Fnombre");
			TextoPintado=TextoPintado+"<font color=\"gray\">&#60/nombre&#62<font>";
			return new Symbol(Simbolo.FNom, yychar,yyline,new String(yytext()));
		}
	"x-nombre"		
		{
			System.out.println("Vino Idocumento");
			TextoPintado=TextoPintado+"<font color=\"red\">&#60documento&#62<font>";
			return new Symbol(Simbolo.IDoc, yychar,yyline,new String(yytext()));
		}
	"x-imagen"		
		{
			System.out.println("Vino Fdocumento");
			TextoPintado=TextoPintado+"<font color=\"red\">&#60/documento&#62<font>";
			return new Symbol(Simbolo.FDoc, yychar,yyline,new String(yytext()));
		}
	"x-tipo"		
		{
			System.out.println("Vino Iformato");
			TextoPintado=TextoPintado+"<font color=\"orange\">&#60formato&#62<font>";
			return new Symbol(Simbolo.IForm, yychar,yyline,new String(yytext()));
		}
	","		
		{
			System.out.println("Vino Fformato");
			TextoPintado=TextoPintado+"<font color=\"orange\">&#60/formato&#62<font>";
			return new Symbol(Simbolo.FForm, yychar,yyline,new String(yytext()));
		}
	";"		
		{
			System.out.println("Vino Icontenido");
			TextoPintado=TextoPintado+"<font color=\"purple\">&#60contenido&#62<font>";
			return new Symbol(Simbolo.ICon, yychar,yyline,new String(yytext()));
		}
	"x-vida"		
		{
			System.out.println("Vino Fcontenido");
			TextoPintado=TextoPintado+"<font color=\"purple\">&#60/contenido&#62<font>";
			return new Symbol(Simbolo.FCon, yychar,yyline,new String(yytext()));
		}
	"x-heroe"		
		{
			System.out.println("Vino Fcontenido");
			TextoPintado=TextoPintado+"<font color=\"purple\">&#60/contenido&#62<font>";
			return new Symbol(Simbolo.FCon, yychar,yyline,new String(yytext()));
		}
	"x-enemigo"		
		{
			System.out.println("Vino Fcontenido");
			TextoPintado=TextoPintado+"<font color=\"purple\">&#60/contenido&#62<font>";
			return new Symbol(Simbolo.FCon, yychar,yyline,new String(yytext()));
		}
	"x-destruir"		
		{
			System.out.println("Vino Fcontenido");
			TextoPintado=TextoPintado+"<font color=\"purple\">&#60/contenido&#62<font>";
			return new Symbol(Simbolo.FCon, yychar,yyline,new String(yytext()));
		}
	"x-descripcion"		
		{
			System.out.println("Vino Fcontenido");
			TextoPintado=TextoPintado+"<font color=\"purple\">&#60/contenido&#62<font>";
			return new Symbol(Simbolo.FCon, yychar,yyline,new String(yytext()));
		}
	"x-meta"		
		{
			System.out.println("Vino Fcontenido");
			TextoPintado=TextoPintado+"<font color=\"purple\">&#60/contenido&#62<font>";
			return new Symbol(Simbolo.FCon, yychar,yyline,new String(yytext()));
		}
	"x-bloque"		
		{
			System.out.println("Vino Fcontenido");
			TextoPintado=TextoPintado+"<font color=\"purple\">&#60/contenido&#62<font>";
			return new Symbol(Simbolo.FCon, yychar,yyline,new String(yytext()));
		}
	"x-bonus"		
		{
			System.out.println("Vino Fcontenido");
			TextoPintado=TextoPintado+"<font color=\"purple\">&#60/contenido&#62<font>";
			return new Symbol(Simbolo.FCon, yychar,yyline,new String(yytext()));
		}
	"x-bomba"		
		{
			System.out.println("Vino Fcontenido");
			TextoPintado=TextoPintado+"<font color=\"purple\">&#60/contenido&#62<font>";
			return new Symbol(Simbolo.FCon, yychar,yyline,new String(yytext()));
		}
	"x-arma"		
		{
			System.out.println("Vino Fcontenido");
			TextoPintado=TextoPintado+"<font color=\"purple\">&#60/contenido&#62<font>";
			return new Symbol(Simbolo.FCon, yychar,yyline,new String(yytext()));
		}
	"x-creditos"		
		{
			System.out.println("Vino Fcontenido");
			TextoPintado=TextoPintado+"<font color=\"purple\">&#60/contenido&#62<font>";
			return new Symbol(Simbolo.FCon, yychar,yyline,new String(yytext()));
		}
	. 	{
			System.out.println("token� ilegal� <" + yytext()+ ">� � linea:� " + (yyline+1) + "� columna:� " + (yycolumn+1));
			i++;
			TextoErrores=TextoErrores+"<tr><th>" +i+ "</th><th>" +yytext()+"</th><th>" +(yyline+1)+ "</th><th>" +(yycolumn+1)+ "</th></tr>";
		}
}