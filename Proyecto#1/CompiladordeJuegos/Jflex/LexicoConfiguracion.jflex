
//* ------------Secci�n codigo-usuario -------- */
package compiladordejuegos;

import java_cup.runtime.*;
import java.util.LinkedList;

%%
/*- Secci�n de opciones y declaraciones -*/
/*-OPCIONES --*/
/* Nombre de la clase generada para el analizador lexico */
%class LexicoConfiguracion
/*Si se quiere que no se tome en cuenta la diferencia entre la mayuscula y la minuscula*/
//%ignorecase
%caseless 
/*Para que el lector del archivo tenga la cadena de 16 bits*/
%unicode
/*Para que las clases sean publicas*/
//%public
/* Indicar funcionamiento autonomo, crea un metodo main para que solo sea llamando*/
//%standalone
/* Acceso a la columna y fila actual de analisis CUP */
%line
%column
/*El nombre de la clase que cup va a generar*/
%cupsym SimbolosConfiguracion
/* Habilitar la compatibilidad con el interfaz CUP para el generador sintactico*/
%cup

%function next_token
%type java_cup.runtime.Symbol
%eofval{
	System.out.println("Fin");
        return new Symbol(SimbolosConfiguracion.EOF);
  
  //return null;
%eofval}
%eofclose

/*-- DECLARACIONES --*/
%{
           LinkedList<NodoSimbolo> ListaSimbolos;
           LinkedList<NodoErrorLexico> ListaErroresLexicos;
	
    private void CrearNodoListaSimbolo(String lexema,int fila, int columna,String token){
        NodoSimbolo simbolo=new NodoSimbolo(lexema,fila+1,columna+1,token);
        ListaSimbolos.add(simbolo);
    }
    private void CrearNodoListaErroresLexicos(String lexema,int fila, int columna){
        NodoErrorLexico simbolo=new NodoErrorLexico(lexema,fila+1,columna+1);
        ListaErroresLexicos.add(simbolo);
    }
    
    public LinkedList<NodoSimbolo> DevolverListaSimbolos(){
        return ListaSimbolos;
    }
    public LinkedList<NodoErrorLexico> DevolverListaErroresLexicos(){
        return ListaErroresLexicos;
    }



	
	/* private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    } */
    
    /* Also creates a new java_cup.runtime.Symbol with information
       about the current token, but this object has a value. */
    /* private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    } */
	
	
%}

%init{ 
            ListaSimbolos=new LinkedList<NodoSimbolo>();
            ListaErroresLexicos=new LinkedList<NodoErrorLexico>() ;
%init}

/*-- MACRO DECLARACIONES --*/
Espacio=[ \t]
Salto=\n
Num=[0-9]
Letras=[a-zA-Z\u00d1\u00f1]
Vida=(0)|([1-9](0))|(100)
Nombre={Letras}({Num}|{Letras}|[_])*
Carpeta=[^\\/:*?<>|\n \t][^\\/:*?<>|\n\t]*
Extension=[\.](jpg|png|gif)
DirImagen = [\"]({Letras}[:])?(([\/]{Carpeta})*)?[\/]{Nombre}{Extension}[\"]
Descripcion=[\"][^\"]*[\"]



%%
/*-------- Secci�n de reglas lexicas ------ */
<YYINITIAL> {
	{Espacio} 	
		{
			//System.out.println("Vino espacio");
		}
	{Salto}		
		{
			//System.out.println("Vino Salto");
		}
        "<configuracion>"		
		{
			//System.out.println("Vino IConfiguracion "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"IConfiguracion");
			return new Symbol(SimbolosConfiguracion.IConfi, yycolumn,yyline,new String(yytext()));
		}
	"</configuracion>"		
		{
			//System.out.println("Vino FConfiguracion "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"FConfiguracion");
			return new Symbol(SimbolosConfiguracion.FConfi, yycolumn,yyline,new String(yytext()));
		}
        "<fondo>"		
		{
			//System.out.println("Vino IFondo "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"IFondo");
			return new Symbol(SimbolosConfiguracion.IFondo, yycolumn,yyline,new String(yytext()));
		}
	"</fondo>"		
		{
			//System.out.println("Vino FFondo "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"FFondo");
			return new Symbol(SimbolosConfiguracion.FFondo, yycolumn,yyline,new String(yytext()));
		}
        "<figura>"		
		{
			//System.out.println("Vino IFigura "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"IFigura");
			return new Symbol(SimbolosConfiguracion.IFigura, yycolumn,yyline,new String(yytext()));
		}
	"</figura>"		
		{
			System.out.println("Vino FFigura "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"FFigura");
			return new Symbol(SimbolosConfiguracion.FFigura, yycolumn,yyline,new String(yytext()));
		}
        "<dise\u00f1o>"		
		{
			System.out.println("Vino IDiseno "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"IDiseno");
			return new Symbol(SimbolosConfiguracion.IDiseno, yycolumn,yyline,new String(yytext()));
		}
	"</dise\u00f1o>"		
		{
			System.out.println("Vino FDiseno "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"FDiseno");
			return new Symbol(SimbolosConfiguracion.FDiseno, yycolumn,yyline,new String(yytext()));
		}
	
	"nombre"		
		{
			System.out.println("Vino DNombre "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"DNombre");
			return new Symbol(SimbolosConfiguracion.DNombre, yycolumn,yyline,new String(yytext()));
		}
        "{"		
		{
			System.out.println("Vino ILlave "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"ILlave");
			return new Symbol(SimbolosConfiguracion.ILlave, yycolumn,yyline,new String(yytext()));
		}
        "}"		
		{
			System.out.println("Vino FLlave "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"FLlave");
			return new Symbol(SimbolosConfiguracion.FLlave, yycolumn,yyline,new String(yytext()));
		}
        ","		
		{
			System.out.println("Vino Coma "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"Coma");
			return new Symbol(SimbolosConfiguracion.Coma, yycolumn,yyline,new String(yytext()));
		}
	";"		
		{
			System.out.println("Vino PuntoComa "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"PuntoComa");
			return new Symbol(SimbolosConfiguracion.PuntoComa, yycolumn,yyline,new String(yytext()));
		}
        "="		
		{
			System.out.println("Vino Igual "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"Igual");
			return new Symbol(SimbolosConfiguracion.Igual, yycolumn,yyline,new String(yytext()));
		}
        "imagen"		
		{
			System.out.println("Vino DImagen "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"DImagen");
			return new Symbol(SimbolosConfiguracion.DImagen, yycolumn,yyline,new String(yytext()));
		}
        {DirImagen}		
		{
			System.out.println("Vino DirImagen "+yytext());
                        String Aux=yytext();
                        Aux=Aux.substring(1, Aux.length()-1);
                        //Aux=Aux.replaceAll("\"", "");
                        CrearNodoListaSimbolo(Aux,yyline,yycolumn,"DirImagen");
			return new Symbol(SimbolosConfiguracion.DirImagen, yycolumn,yyline,Aux);
		}
        "vida"		
		{
			System.out.println("Vino DVida "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"DVida");
			return new Symbol(SimbolosConfiguracion.DVida, yycolumn,yyline,new String(yytext()));
		}
        {Vida}		
		{
			System.out.println("Vino Vida "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"Vida");
			return new Symbol(SimbolosConfiguracion.Vida, yycolumn,yyline,new String(yytext()));
		}
        "tipo"		
		{
			System.out.println("Vino DTipo "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"DTipo");
			return new Symbol(SimbolosConfiguracion.DTipo, yycolumn,yyline,new String(yytext()));
		}
        "heroe"		
		{
			System.out.println("Vino THeroe "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"THeroe");
			return new Symbol(SimbolosConfiguracion.THeroe, yycolumn,yyline,new String(yytext()));
		}
        "enemigo"		
		{
			System.out.println("Vino TEnemigo "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"TEnemigo");
			return new Symbol(SimbolosConfiguracion.TEnemigo, yycolumn,yyline,new String(yytext()));
		}
        "destruir"		
		{
			System.out.println("Vino DDestruir "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"DDestruir");
			return new Symbol(SimbolosConfiguracion.DDestruir, yycolumn,yyline,new String(yytext()));
		}
        "descripcion"		
		{
			System.out.println("Vino DDescripcion "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"DDescripcion");
			return new Symbol(SimbolosConfiguracion.DDescripcion, yycolumn,yyline,new String(yytext()));
		}
        {Descripcion}		
		{
			System.out.println("Vino Descripcion "+yytext());
                        String Aux=yytext();
                        Aux=Aux.substring(1, Aux.length()-1);
                        CrearNodoListaSimbolo(Aux,yyline,yycolumn,"Descripcion");
			return new Symbol(SimbolosConfiguracion.Descripcion, yycolumn,yyline,Aux);
		}
	"meta"		
		{
			System.out.println("Vino TMeta "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"TMeta");
			return new Symbol(SimbolosConfiguracion.TMeta, yycolumn,yyline,new String(yytext()));
		}
	"bloque"		
		{
			System.out.println("Vino TBloque "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"TBloque");
			return new Symbol(SimbolosConfiguracion.TBloque, yycolumn,yyline,new String(yytext()));
		}
	"bonus"		
		{
			System.out.println("Vino TBonus "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"TBonus");
			return new Symbol(SimbolosConfiguracion.TBonus, yycolumn,yyline,new String(yytext()));
		}
	"bomba"		
		{
			System.out.println("Vino TBomba "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"TBomba");
			return new Symbol(SimbolosConfiguracion.TBomba, yycolumn,yyline,new String(yytext()));
		}
	"arma"		
		{
			System.out.println("Vino TArma "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"TArma");
			return new Symbol(SimbolosConfiguracion.TArma, yycolumn,yyline,new String(yytext()));
		}
	"creditos"		
		{
			System.out.println("Vino DCreditos "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"DCreditos");
			return new Symbol(SimbolosConfiguracion.DCreditos, yycolumn,yyline,new String(yytext()));
		}
        {Nombre}		
		{
			System.out.println("Vino Nombre "+yytext());
                        CrearNodoListaSimbolo(yytext(),yyline,yycolumn,"Nombre");
                        return new Symbol(SimbolosConfiguracion.Nombre,yycolumn,yyline,new String(yytext()));
		}
	. 	{
			System.out.println("token ilegal < " + yytext()+ " > linea: " + (yyline+1) + "� columna:� " + (yycolumn+1));
			CrearNodoListaErroresLexicos(yytext(),yyline, yycolumn);
		}
}